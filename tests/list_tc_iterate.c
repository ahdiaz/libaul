/**
 * libaul
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#include <stdlib.h>
#include <stdio.h>
#include <check.h>

#include "../src/aul.h"


/**
 * Creates a new MNode
 */
START_TEST(test_list_iterate)
{
    List *list = NULL;
    Node *node = NULL;
    unsigned short i = 0;
    char value[7];

    list = list_new();
    ck_assert_ptr_nonnull(list);
    ck_assert_int_eq(list_is_empty(list), 1);

    list_append(list, "value1", 0, NULL);
    list_append(list, "value2", 0, NULL);
    list_append(list, "value3", 0, NULL);
    list_append(list, "value4", 0, NULL);

    ck_assert_int_eq(list_len(list), 4);

    node = list_first(list);

    do {
        sprintf(value, "value%hu", ++i);
        ck_assert_str_eq(node_value(node), value);
        node = list_next(node);
    } while (node);

    ck_assert_ptr_null(node);

    list_free(&list);
}
END_TEST

int
_iterator_callback(Node *node, List *list, List *new_list, void *user_data)
{
    char value[7];
    int *i = (int*) user_data;

    sprintf(value, "value%d", *i);
    ck_assert_str_eq(node_value(node), value);

    // Check the integer is incrementing outside this function
    (*i)++;

    // Continue with the loop
    return 0;
}

/**
 * Creates a new MNode
 */
START_TEST(test_list_iterate_callbacks)
{
    List *list = NULL;
    int i = 1;

    list = list_new();
    ck_assert_ptr_nonnull(list);
    ck_assert_int_eq(list_is_empty(list), 1);

    list_append(list, "value1", 0, NULL);
    list_append(list, "value2", 0, NULL);
    list_append(list, "value3", 0, NULL);
    list_append(list, "value4", 0, NULL);

    ck_assert_int_eq(list_len(list), 4);

    list_foreach(list, _iterator_callback, &i);

    // BTW, let's check that i has been incrementing
    // its value from inside the callback.
    ck_assert_int_eq(i, 5);

    list_free(&list);
}
END_TEST

void
make_list_tcase_iterate(Suite *s)
{
    TCase *tc;

    tc = tcase_create("Iterate list");
    tcase_add_test(tc, test_list_iterate);
    tcase_add_test(tc, test_list_iterate_callbacks);

    suite_add_tcase(s, tc);
}
