/**
 * libaul
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#include <stdlib.h>
#include <stdio.h>
#include <check.h>

#include "tests.h"
#include "../src/aul.h"


int
_test_list_filter_iterator(Node *node, List *list, List *new_list, void *user_data)
{
    int value = *(int*) node_value(node);
    return value < 0 ? 1 : 0;
}

/**
 * Creates a new MNode
 */
START_TEST(test_list_filter)
{
    List *list = NULL, *filtered = NULL;
    int nums[] = {3, -2, 77, -87, 132, -200};
    int index = 0;

    list = list_new();
    ck_assert_ptr_nonnull(list);

    for (index = 0; index < 6; index++) {
        list_append(list, &nums[index], sizeof(int), NULL);
    }

    ck_assert_int_eq(list_len(list), 6);

    filtered = list_filter(list, _test_list_filter_iterator, NULL);

    ck_assert_ptr_nonnull(filtered);
    ck_assert_int_eq(list_len(filtered), 3);

    for (index = 0; index < 3; index++) {
        ck_assert_int_gt( *(int*) list_value_nth(filtered, index) , 0);
    }

    list_free(&list);
    list_free(&filtered);
}
END_TEST

Node*
_test_list_map_iterator_1(Node *node, List *list, List *new_list, void *user_data)
{
    Node *mapped = node_clone(node);
    int *value = (int*) node_value(node);

    *value *= -1;
    mapped->data = value;
    mapped->d_size = sizeof(int*);

    return mapped;
}

/**
 * Creates a new MNode
 */
START_TEST(test_list_map_1)
{
    List *list = NULL, *mapped = NULL;
    int nums[] = {3, -2, 77, -87, 132, -200};
    int index = 0;

    list = list_new();
    ck_assert_ptr_nonnull(list);

    for (index = 0; index < 6; index++) {
        list_append(list, &nums[index], sizeof(int), NULL);
    }

    ck_assert_int_eq(list_len(list), 6);

    mapped = list_map(list, _test_list_map_iterator_1, NULL);

    ck_assert_ptr_nonnull(mapped);
    ck_assert_int_eq(list_len(mapped), 6);

    index = *(int*)list_value_nth(mapped, 0);
    ck_assert_int_eq(index, -3);

    index = *(int*)list_value_nth(mapped, 1);
    ck_assert_int_eq(index, 2);

    index = *(int*)list_value_nth(mapped, 2);
    ck_assert_int_eq(index, -77);

    index = *(int*)list_value_nth(mapped, 3);
    ck_assert_int_eq(index, 87);

    index = *(int*)list_value_nth(mapped, 4);
    ck_assert_int_eq(index, -132);

    index = *(int*)list_value_nth(mapped, 5);
    ck_assert_int_eq(index, 200);

    list_free(&list);
    list_free(&mapped);
}
END_TEST

Node*
_test_list_map_iterator_2(Node *node, List *list, List *new_list, void *user_data)
{
    Node *mapped = node_clone(node);
    Point *p = node_value(node);
    char *buf = malloc(50);

    sprintf(buf, "point: x = %d, y = %d", p->x, p->y);
    mapped->data = buf;
    mapped->d_size = sizeof(buf);

    return mapped;
}

/**
 * Creates a new MNode
 */
START_TEST(test_list_map_2)
{
    List *list = NULL, *mapped = NULL;
    Point *p1 = NULL, *p2 = NULL;
    char *value = NULL;

    p1 = point_new(3, 4);
    p2 = point_new(77, -99);

    list = list_new();
    ck_assert_ptr_nonnull(list);

    list_append(list, p1, sizeof(Point), point_free);
    list_append(list, p2, sizeof(Point), point_free);

    ck_assert_int_eq(list_len(list), 2);

    mapped = list_map(list, _test_list_map_iterator_2, NULL);

    ck_assert_ptr_nonnull(mapped);
    ck_assert_int_eq(list_len(mapped), 2);

    value = list_value_nth(mapped, 0);
    ck_assert_str_eq(value, "point: x = 3, y = 4");

    value = list_value_nth(mapped, 1);
    ck_assert_str_eq(value, "point: x = 77, y = -99");

    list_free(&list);
    list_free(&mapped);
}
END_TEST

void
make_list_tcase_filters(Suite *s)
{
    TCase *tc;

    tc = tcase_create("Node filters");
    tcase_add_test(tc, test_list_filter);
    tcase_add_test(tc, test_list_map_1);
    tcase_add_test(tc, test_list_map_2);

    suite_add_tcase(s, tc);
}
