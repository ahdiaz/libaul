/**
 * libaul
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#include <stdlib.h>
#include <check.h>

#include "tests.h"
#include "../src/aul.h"


/**
 * Test retrieve nodes and ites values
 */
START_TEST(test_list_nodes_and_values)
{
    List *list = NULL;

    list = list_new();
    ck_assert_ptr_nonnull(list);
    ck_assert_int_eq(list_is_empty(list), 1);

    list_append(list, "value1", sizeof("value1"), NULL);
    list_append(list, "value2", sizeof("value2"), NULL);
    list_append(list, "value3", sizeof("value3"), NULL);

    ck_assert_int_eq(list_len(list), 3);

    ck_assert_str_eq(node_value(list_node_nth(list, 0)), "value1");
    ck_assert_str_eq(node_value(list_node_nth(list, 1)), "value2");
    ck_assert_str_eq(node_value(list_node_nth(list, 2)), "value3");
    ck_assert_ptr_null(node_value(list_node_nth(list, 3)));

    ck_assert_str_eq(list_value_nth(list, 0), "value1");
    ck_assert_str_eq(list_value_nth(list, 1), "value2");
    ck_assert_str_eq(list_value_nth(list, 2), "value3");
    ck_assert_ptr_null(list_value_nth(list, 3));

    list_free(&list);
    ck_assert_ptr_null(list);
}
END_TEST

Suite *
make_list_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("Lists");

    tc_core = tcase_create("Lists");

    tcase_add_test(tc_core, test_list_nodes_and_values);
    suite_add_tcase(s, tc_core);

    make_list_tcase_insert(s);
    make_list_tcase_remove(s);
    make_list_tcase_iterate(s);
    make_list_tcase_operations(s);
    make_list_tcase_filters(s);

    return s;
}
