/**
 * libaul
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#include <stdlib.h>
#include <check.h>
#include "tests.h"

Point*
point_new(int x, int y)
{
    Point *point;
    point = malloc(sizeof(Point));
    point->x = x;
    point->y = y;
    return point;
}

void
point_free(void *point)
{
    Point *p = (Point*) point;
    free(p);
}

int main(void)
{
    int number_failed;
    SRunner *sr;

    Suite *s1 = make_string_suite();
    Suite *s2 = make_list_suite();
    Suite *s3 = make_hash_suite();

    sr = srunner_create(s1);
    srunner_add_suite(sr, s2);
    srunner_add_suite(sr, s3);

    srunner_run_all(sr, CK_VERBOSE);

    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

