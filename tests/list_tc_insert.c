/**
 * libaul
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#include <stdlib.h>
#include <check.h>

#include "../src/aul.h"


/**
 * Creates a new MNode
 */
START_TEST(test_list_movements)
{
    List *list = list_new();

    ck_assert_ptr_nonnull(list);

    list_append(list, "value1", 0, NULL);
    list_append(list, "value2", 0, NULL);
    list_append(list, "value3", 0, NULL);

    ck_assert_int_eq(list_len(list), 3);

    Node *node1 = list_node_nth(list, 0);
    Node *node2 = list_node_nth(list, 1);
    Node *node3 = list_node_nth(list, 2);

    ck_assert_int_eq(list_is_first(node1), 1);
    ck_assert_int_eq(list_is_last(node3), 1);

    ck_assert_ptr_eq(node1, list_first(list));
    ck_assert_ptr_eq(node3, list_last(list));

    ck_assert_ptr_eq(list_next(node1), node2);
    ck_assert_ptr_eq(list_prev(node3), node2);

    ck_assert_int_eq(list_has_prev(node1), 0);
    ck_assert_int_eq(list_has_next(node1), 1);
    ck_assert_int_eq(list_has_prev(node2), 1);
    ck_assert_int_eq(list_has_next(node2), 1);
    ck_assert_int_eq(list_has_prev(node3), 1);
    ck_assert_int_eq(list_has_next(node3), 0);

    list_free(&list);
}
END_TEST

/**
 * Creates a new MNode
 */
START_TEST(test_list_append)
{
    List *list = NULL;

    list = list_new();
    ck_assert_ptr_nonnull(list);

    list_append(list, "value1", 0, NULL);
    list_append(list, "value2", 0, NULL);
    list_append(list, "value3", 0, NULL);

    ck_assert_int_eq(list_len(list), 3);

    ck_assert_str_eq(list_value_nth(list, 0), "value1");
    ck_assert_str_eq(list_value_nth(list, 1), "value2");
    ck_assert_str_eq(list_value_nth(list, 2), "value3");

    list_free(&list);
}
END_TEST

/**
 * Creates a new MNode
 */
START_TEST(test_list_append_int)
{
    List *list = NULL;
    Node *node = NULL;
    int values[] = {1, 2};
    int val1 = 0;
    int *val2 = NULL;

    list = list_new();
    ck_assert_ptr_nonnull(list);

    list_append(list, &values[0], 0, NULL);
    list_append(list, &values[1], 0, NULL);

    ck_assert_int_eq(list_len(list), 2);

    node = list_first(list);
    val1 = *(int*)(node_value(node));

    ck_assert_int_eq(val1, values[0]);

    node = list_last(list);
    val2 = node_value(node);

    ck_assert_int_eq(*val2, values[1]);

    list_free(&list);
}
END_TEST

/**
 * Creates a new MNode
 */
START_TEST(test_list_prepend)
{
    List *list = NULL;

    list = list_new();
    ck_assert_ptr_nonnull(list);

    list_prepend(list, "value1", 0, NULL);
    list_prepend(list, "value2", 0, NULL);
    list_prepend(list, "value3", 0, NULL);

    ck_assert_int_eq(list_len(list), 3);

    ck_assert_str_eq(list_value_nth(list, 0), "value3");
    ck_assert_str_eq(list_value_nth(list, 1), "value2");
    ck_assert_str_eq(list_value_nth(list, 2), "value1");

    list_free(&list);
}
END_TEST

/**
 * Creates a new MNode
 */
START_TEST(test_list_insert)
{
    List *list = NULL;
    Node *node = NULL;

    list = list_new();
    ck_assert_ptr_nonnull(list);

    // No matter what the index is, first item is the first
    node = list_insert(list, "value1", 0, 77, NULL);
    ck_assert_int_eq(list_has_prev(node), 0);
    ck_assert_int_eq(list_has_next(node), 0);

    // Lower out bound index will prepend the item
    node = list_insert(list, "value2", 0, -77, NULL);
    ck_assert_int_eq(list_has_prev(node), 0);
    ck_assert_int_eq(list_has_next(node), 1);

    // Upper out bound index will append the item
    node = list_insert(list, "value3", 0, 77, NULL);
    ck_assert_int_eq(list_has_prev(node), 1);
    ck_assert_int_eq(list_has_next(node), 0);

    // At this point the list is:
    // value2 - value1 - value3

    // This insert the new item in the second position
    node = list_insert(list, "value4", 0, 0, NULL);
    ck_assert_int_eq(list_has_prev(node), 1);
    ck_assert_int_eq(list_has_next(node), 1);

    // This insert the new item in the last position
    node = list_insert(list, "value5", 0, list_len(list) - 1, NULL);
    ck_assert_int_eq(list_has_prev(node), 1);
    ck_assert_int_eq(list_has_next(node), 0);

    // At this point the list is:
    // value2 - value4 - value1 - value3 - value5

    // This insert the new item after value1
    node = list_insert(list, "value6", 0, 2, NULL);
    ck_assert_int_eq(list_has_prev(node), 1);
    ck_assert_int_eq(list_has_next(node), 1);

    // The final list:
    // value2 - value4 - value1 - value6 - value3 - value5

    ck_assert_int_eq(list_len(list), 6);

    ck_assert_str_eq(list_value_nth(list, 0), "value2");
    ck_assert_str_eq(list_value_nth(list, 1), "value4");
    ck_assert_str_eq(list_value_nth(list, 2), "value1");
    ck_assert_str_eq(list_value_nth(list, 3), "value6");
    ck_assert_str_eq(list_value_nth(list, 4), "value3");
    ck_assert_str_eq(list_value_nth(list, 5), "value5");

    list_free(&list);
}
END_TEST

void
make_list_tcase_insert(Suite *s)
{
    TCase *tc;

    tc = tcase_create("Insert nodes");
    tcase_add_test(tc, test_list_movements);
    tcase_add_test(tc, test_list_append);
    tcase_add_test(tc, test_list_append_int);
    tcase_add_test(tc, test_list_prepend);
    tcase_add_test(tc, test_list_insert);

    suite_add_tcase(s, tc);
}
