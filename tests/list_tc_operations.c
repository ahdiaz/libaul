/**
 * libaul
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#include <stdlib.h>
#include <check.h>

#include "tests.h"
#include "../src/aul.h"


/**
 * Creates a new MNode
 */
START_TEST(test_list_search)
{
    List *list = NULL;
    Node *node = NULL;

    list = list_new();
    ck_assert_ptr_nonnull(list);
    ck_assert_int_eq(list_is_empty(list), 1);

    list_append(list, "value1", 0, NULL);
    list_append(list, "value2", 0, NULL);
    list_append(list, "value3", 0, NULL);
    list_append(list, "value4", 0, NULL);

    ck_assert_int_eq(list_len(list), 4);

    node = list_search(list, "value4", 0, NULL);
    ck_assert_ptr_nonnull(node);
    ck_assert_str_eq(node_value(node), "value4");

    node = list_search(list, "foobar", 0, NULL);
    ck_assert_ptr_null(node);

    node = list_search(list, "value1", 0, NULL);
    ck_assert_ptr_nonnull(node);
    ck_assert_str_eq(node_value(node), "value1");

    list_free(&list);
}
END_TEST

START_TEST(test_list_search_2)
{
    List *list = NULL;
    Node *node = NULL;

    list = list_new();
    ck_assert_ptr_nonnull(list);
    ck_assert_int_eq(list_is_empty(list), 1);

    list_append(list, "value1", 0, NULL);
    list_append(list, "foobar", 0, NULL);
    list_append(list, "value3", 0, NULL);
    list_append(list, "foobar", 0, NULL);
    list_append(list, "value4", 0, NULL);
    list_append(list, "foobar", 0, NULL);

    ck_assert_int_eq(list_len(list), 6);

    node = list_search(list, "value4", 0, NULL);
    ck_assert_ptr_nonnull(node);
    ck_assert_str_eq(node_value(node), "value4");

    node = list_search(list, "foobar", 0, NULL);
    ck_assert_ptr_nonnull(node);
    ck_assert_int_eq(list_index_of(list, node->data, 0, NULL), 1);

    node = list_search(list, "foobar", 4, NULL);
    ck_assert_ptr_nonnull(node);
    ck_assert_int_eq(list_index_of(list, node->data, 4, NULL), 5);

    node = list_search(list, "value1", 0, NULL);
    ck_assert_ptr_nonnull(node);
    ck_assert_str_eq(node_value(node), "value1");

    list_free(&list);
}
END_TEST

int
_compare_func_char(void *needle, void *item)
{
    char *str1 = (char*) needle;
    char *str2 = (char*) item;

    return strcmp(str1, str2) == 0;
}

int
_compare_func_int(void *needle, void *item)
{
    int *i1 = (int*) needle;
    int *i2 = (int*) item;

    return *i1 == *i2;
}

/**
 * Creates a new MNode
 */
START_TEST(test_list_search_custom)
{
    List *list = NULL;
    Node *node = NULL;

    list = list_new();
    ck_assert_ptr_nonnull(list);
    ck_assert_int_eq(list_is_empty(list), 1);

    list_append(list, "value1", 0, NULL);
    list_append(list, "value2", 0, NULL);
    list_append(list, "value3", 0, NULL);
    list_append(list, "value4", 0, NULL);

    ck_assert_int_eq(list_len(list), 4);

    node = list_search(list, "value4", 0, _compare_func_char);
    ck_assert_ptr_nonnull(node);
    ck_assert_str_eq(node_value(node), "value4");

    node = list_search(list, "foobar", 0, _compare_func_char);
    ck_assert_ptr_null(node);

    list_free(&list);
}
END_TEST

/**
 * Creates a new MNode
 */
START_TEST(test_list_search_custom_2)
{
    List *list = NULL;
    Node *node = NULL;
    int ns[4] = {33, 77, 23, 105};
    int ns2[2] = {77, -200};

    list = list_new();
    ck_assert_ptr_nonnull(list);
    ck_assert_int_eq(list_is_empty(list), 1);

    list_append(list, &ns[0], 0, NULL);
    list_append(list, &ns[1], 0, NULL);
    list_append(list, &ns[2], 0, NULL);
    list_append(list, &ns[3], 0, NULL);

    ck_assert_int_eq(list_len(list), 4);

    node = list_search(list, &ns2[0], 0, _compare_func_int);
    ck_assert_ptr_nonnull(node);
    ck_assert_int_eq(*(int*)node_value(node), ns2[0]);

    node = list_search(list, &ns2[1], 0, _compare_func_int);
    ck_assert_ptr_null(node);

    list_free(&list);
}
END_TEST

/**
 * Creates a new MNode
 */
START_TEST(test_list_clone)
{
    List *l1, *l2, *l3;
    List *c1, *c2, *c3;

    l1 = l2 = l3 = NULL;
    c1 = c2 = c3 = NULL;

    Point *p, *cp;

    p = cp = NULL;

    int n = 77, cn = 0;
    char *s = "a string", *cs = NULL;


    p = point_new(3, 4);

    // Create 3 lists with different node values
    l1 = list_new();
    list_append(l1, p, sizeof(Point), point_free);

    // Clone the first one and check that addresses are different
    c1 = list_clone(l1);
    ck_assert_ptr_nonnull(c1);
    ck_assert_mem_ne(l1, c1, sizeof(List));

    // Check that the point node in the first list is equals to the cloned one
    cp = (Point*) node_value(list_first(c1));
    ck_assert_int_eq(cp->x, p->x);
    ck_assert_int_eq(cp->y, p->y);

    // Modify the first point and check the cloned one doesn't change its value
    p->x += 2;
    p->y += 2;
    ck_assert_int_ne(cp->x, p->x);
    ck_assert_int_ne(cp->y, p->y);


    l2 = list_new();
    list_append(l2, &n, sizeof(int), NULL);

    // Clone the second list
    c2 = list_clone(l2);
    ck_assert_ptr_nonnull(c2);
    ck_assert_mem_ne(l2, c2, sizeof(List));

    cn = *(int*) node_value(list_first(c2));
    ck_assert_int_eq(cn, n);

    // Modify an integer and check the cloned doesn't change is value
    n += 2;
    ck_assert_int_ne(cn, n);


    l3 = list_new();
    list_append(l3, s, sizeof(s), NULL);

    // Clone the third list
    c3 = list_clone(l3);
    ck_assert_ptr_nonnull(c3);
    ck_assert_mem_ne(l3, c3, sizeof(List));

    cs = (char*) node_value(list_first(c3));
    ck_assert_str_eq(cs, s);

    // Modify the string value and check the cloned one doesn't change
    s = "foobar";
    ck_assert_str_ne(cs, s);

    list_free(&l1);
    list_free(&l2);
    list_free(&l3);
    list_free(&c1);
    list_free(&c2);
    list_free(&c3);
}
END_TEST

START_TEST(test_list_reverse)
{
   List *list = NULL, *reverse = NULL;
   Node *node1 = NULL, *node2 = NULL;

   list = list_new();
   list_append(list, "value1", sizeof("value1"), NULL);
   ck_assert_ptr_nonnull(list);

   list_append(list, "value2", sizeof("value2"), NULL);
   list_append(list, "value3", sizeof("value3"), NULL);
   list_append(list, "value4", sizeof("value4"), NULL);

   ck_assert_int_eq(list_len(list), 4);

   reverse = list_reverse(list);
   ck_assert_ptr_nonnull(reverse);
   ck_assert_int_eq(list_len(reverse), 4);
   ck_assert_mem_ne(list, reverse, sizeof(List));

   node1 = list_first(list);
   node2 = list_last(reverse);
   ck_assert_str_eq(node_value(node1), node_value(node2));

   node1 = list_next(node1);
   node2 = list_prev(node2);
   ck_assert_str_eq(node_value(node1), node_value(node2));

   node1 = list_next(node1);
   node2 = list_prev(node2);
   ck_assert_str_eq(node_value(node1), node_value(node2));

   node1 = list_next(node1);
   node2 = list_prev(node2);
   ck_assert_str_eq(node_value(node1), node_value(node2));

   list_free(&list);
   list_free(&reverse);
}
END_TEST

START_TEST(test_list_unique)
{
    List *list = NULL, *unique = NULL;

    list = list_new();
    ck_assert_ptr_nonnull(list);

    list_append(list, "value1", sizeof("value1"), NULL);
    list_append(list, "value2", sizeof("value2"), NULL);
    list_append(list, "value1", sizeof("value1"), NULL);
    list_append(list, "value2", sizeof("value2"), NULL);

    ck_assert_int_eq(list_len(list), 4);

    unique = list_unique(list, _compare_func_char);

    ck_assert_ptr_nonnull(unique);
    ck_assert_int_eq(list_len(unique), 2);

    ck_assert_str_eq(list_value_nth(unique, 0), "value1");
    ck_assert_str_eq(list_value_nth(unique, 1), "value2");
    ck_assert_ptr_null(list_value_nth(unique, 2));

    list_free(&list);
    list_free(&unique);
}
END_TEST

START_TEST(test_list_unique_int)
{
    List *list = NULL, *unique = NULL;
    int nums[] = {77, 88};

    list = list_new();
    ck_assert_ptr_nonnull(list);

    list_append(list, &nums[0], sizeof(int), NULL);
    list_append(list, &nums[1], sizeof(int), NULL);
    list_append(list, &nums[0], sizeof(int), NULL);
    list_append(list, &nums[1], sizeof(int), NULL);

    ck_assert_int_eq(list_len(list), 4);

    unique = list_unique(list, _compare_func_int);

    ck_assert_ptr_nonnull(unique);
    ck_assert_int_eq(list_len(unique), 2);

    ck_assert_int_eq(*(int*)list_value_nth(unique, 0), nums[0]);
    ck_assert_int_eq(*(int*)list_value_nth(unique, 1), nums[1]);
    ck_assert_ptr_null(list_value_nth(unique, 2));

    list_free(&list);
    list_free(&unique);
}
END_TEST

int
_compare_func_pointer(void *needle, void *item)
{
    Point *p1 = (Point*) needle;
    Point *p2 = (Point*) item;

    return p1->x == p2->x && p1->y == p2->y;
}

START_TEST(test_list_unique_struct)
{
    List *list = NULL, *unique = NULL;
    Point *p1 = NULL, *p2 = NULL, *p3 = NULL;

    p1 = point_new(3, 4);
    p2 = point_new(77, 88);

    list = list_new(point_free);
    ck_assert_ptr_nonnull(list);

    list_append(list, p1, sizeof(Point), NULL);
    list_append(list, p2, sizeof(Point), NULL);
    list_append(list, p1, sizeof(Point), NULL);
    list_append(list, p2, sizeof(Point), NULL);

    ck_assert_int_eq(list_len(list), 4);

    unique = list_unique(list, _compare_func_pointer);

    ck_assert_ptr_nonnull(unique);
    ck_assert_int_eq(list_len(unique), 2);

    p3 = (Point*) list_value_nth(unique, 0);
    ck_assert_int_eq(p3->x, p1->x);
    ck_assert_int_eq(p3->y, p1->y);

    p3 = (Point*) list_value_nth(unique, 1);
    ck_assert_int_eq(p3->x, p2->x);
    ck_assert_int_eq(p3->y, p2->y);

    ck_assert_ptr_null(list_value_nth(unique, 2));

    list_free(&list);
    list_free(&unique);
}
END_TEST

START_TEST(test_list_merge)
{
    List *list1 = NULL, *list2 = NULL, *merge = NULL;

    list1 = list_new();
    ck_assert_ptr_nonnull(list1);

    list_append(list1, "list1-1", sizeof("list1-1"), NULL);
    list_append(list1, "list1-2", sizeof("list1-2"), NULL);

    ck_assert_int_eq(list_len(list1), 2);


    list2 = list_new();
    ck_assert_ptr_nonnull(list2);

    list_append(list2, "list2-1", sizeof("list2-1"), NULL);
    list_append(list2, "list2-2", sizeof("list2-2"), NULL);
    list_append(list2, "list2-3", sizeof("list2-3"), NULL);

    ck_assert_int_eq(list_len(list2), 3);


    merge = list_merge(list1, list2);

    ck_assert_ptr_nonnull(merge);
    ck_assert_int_eq(list_len(merge), 5);

    ck_assert_str_eq(list_value_nth(merge, 0), "list1-1");
    ck_assert_str_eq(list_value_nth(merge, 1), "list1-2");
    ck_assert_str_eq(list_value_nth(merge, 2), "list2-1");
    ck_assert_str_eq(list_value_nth(merge, 3), "list2-2");
    ck_assert_str_eq(list_value_nth(merge, 4), "list2-3");

    list_free(&list1);
    list_free(&list2);
    list_free(&merge);
}
END_TEST

START_TEST(test_list_diff)
{
    List *list1 = NULL, *list2 = NULL, *diff = NULL;

    list1 = list_new();
    ck_assert_ptr_nonnull(list1);

    list_append(list1, "value1", sizeof("value1"), NULL);
    list_append(list1, "foobar", sizeof("foobar"), NULL);
    list_append(list1, "value2", sizeof("value2"), NULL);

    ck_assert_int_eq(list_len(list1), 3);

    list2 = list_new();
    ck_assert_ptr_nonnull(list2);

    list_append(list2, "value1", sizeof("value1"), NULL);
    list_append(list2, "value2", sizeof("value2"), NULL);

    ck_assert_int_eq(list_len(list2), 2);

    diff = list_diff(list1, list2, _compare_func_char);

    ck_assert_ptr_nonnull(diff);
    ck_assert_int_eq(list_len(diff), 1);

    ck_assert_str_eq(list_value_nth(diff, 0), "foobar");

    list_free(&list1);
    list_free(&list2);
    list_free(&diff);
}
END_TEST

START_TEST(test_list_intersect)
{
    List *list1 = NULL, *list2 = NULL, *intersect = NULL;

    list1 = list_new();
    ck_assert_ptr_nonnull(list1);

    list_append(list1, "value1", sizeof("value1"), NULL);
    list_append(list1, "foobar", sizeof("foobar"), NULL);
    list_append(list1, "value2", sizeof("value2"), NULL);

    ck_assert_int_eq(list_len(list1), 3);

    list2 = list_new();
    ck_assert_ptr_nonnull(list2);

    list_append(list2, "value1", sizeof("value1"), NULL);
    list_append(list2, "value2", sizeof("value2"), NULL);

    ck_assert_int_eq(list_len(list2), 2);

    intersect = list_intersect(list1, list2, _compare_func_char);

    ck_assert_ptr_nonnull(intersect);
    ck_assert_int_eq(list_len(intersect), 2);

    ck_assert_str_eq(list_value_nth(intersect, 0), "value1");
    ck_assert_str_eq(list_value_nth(intersect, 1), "value2");

    list_free(&list1);
    list_free(&list2);
    list_free(&intersect);
}
END_TEST

void
make_list_tcase_operations(Suite *s)
{
    TCase *tc;

    tc = tcase_create("Node operations");
    tcase_add_test(tc, test_list_search);
    tcase_add_test(tc, test_list_search_2);
    tcase_add_test(tc, test_list_search_custom);
    tcase_add_test(tc, test_list_search_custom_2);
    tcase_add_test(tc, test_list_clone);
    tcase_add_test(tc, test_list_reverse);
    tcase_add_test(tc, test_list_unique);
    tcase_add_test(tc, test_list_unique_int);
    tcase_add_test(tc, test_list_unique_struct);
    tcase_add_test(tc, test_list_merge);
    tcase_add_test(tc, test_list_diff);
    tcase_add_test(tc, test_list_intersect);

    suite_add_tcase(s, tc);
}
