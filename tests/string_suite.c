/**
 * libaul
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#include <stdlib.h>
#include <check.h>

// #include "string_suite.h"
#include "../src/aul.h"


/**
 * Creates a new buffer
 */
START_TEST(test_string_create)
{
    String *str_a = str_new();
    String *str_b = str_new_set("str_b");

    ck_assert_int_eq(str_len(str_a), 0);
    ck_assert_str_eq(str_value(str_a), "");

    ck_assert_int_eq(str_len(str_b), 5);
    ck_assert_str_eq(str_value(str_b), "str_b");

    str_free(&str_a);
    str_free(&str_b);

   ck_assert_ptr_null(str_a);
   ck_assert_ptr_null(str_b);
}
END_TEST

/**
 * Clears a buffer
 */
START_TEST(test_string_clear)
{
    String *sb = str_new_set("1234");

    ck_assert_str_eq(str_value(sb), "1234");

    str_clear(sb);

    ck_assert_str_eq(str_value(sb), "");

    str_free(&sb);
}
END_TEST

/**
 * Appending text strings
 */
START_TEST(test_string_append)
{
    String *sb = str_new();

    ck_assert_str_eq(str_value(sb), "");

    str_append(sb, "");
    ck_assert_str_eq(str_value(sb), "");

    str_append(sb, "1234");
    ck_assert_str_eq(str_value(sb), "1234");

    str_nappend(sb, "", 0);
    ck_assert_str_eq(str_value(sb), "1234");

    str_nappend(sb, "5678", 4);
    ck_assert_str_eq(str_value(sb), "12345678");

    ck_assert_int_eq(str_len(sb), 8);

    str_free(&sb);
}
END_TEST

/**
 * Setting text strings
 */
START_TEST(test_string_set)
{
    String *sb = str_new();

    ck_assert_str_eq(str_value(sb), "");

    str_set(sb, "");
    ck_assert_str_eq(str_value(sb), "");

    str_set(sb, "1234");
    ck_assert_str_eq(str_value(sb), "1234");

    str_nset(sb, "", 0);
    ck_assert_str_eq(str_value(sb), "");

    str_nset(sb, "5678", 4);
    ck_assert_str_eq(str_value(sb), "5678");

    ck_assert_int_eq(str_len(sb), 4);

    str_free(&sb);
}
END_TEST

/**
 * Check two buffers are equal
 */
START_TEST(test_string_equals)
{
    int equal = 0;
    String *str_a = str_new_set("1234");
    String *str_b = str_new_set("5678");

    ck_assert_str_eq(str_value(str_a), "1234");
    ck_assert_str_eq(str_value(str_b), "5678");

    equal = str_equals(str_a, str_b);
    ck_assert_int_eq(equal, 0);

    str_set(str_a, "AAA");
    str_set(str_b, "AAA");

    ck_assert_str_eq(str_value(str_a), "AAA");
    ck_assert_str_eq(str_value(str_b), "AAA");

    equal = str_equals(str_a, str_b);
    ck_assert_int_eq(equal, 1);

    str_free(&str_a);
    str_free(&str_b);
}
END_TEST

/**
 * Check a cloned buffers is equal to its parent
 */
START_TEST(test_string_clone)
{
    int equal = 0;
    String *str_a = str_new_set("1234");
    String *str_b = NULL;

    ck_assert_str_eq(str_value(str_a), "1234");

    str_b = str_clone(str_a);

    ck_assert_str_eq(str_value(str_b), "1234");

    equal = str_equals(str_a, str_b);
    ck_assert_int_eq(equal, 1);

    str_free(&str_a);
    str_free(&str_b);
}
END_TEST

Suite*
make_string_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("Strings");

    /* Core test case */
    tc_core = tcase_create("Strings");

    tcase_add_test(tc_core, test_string_create);
    tcase_add_test(tc_core, test_string_clear);
    tcase_add_test(tc_core, test_string_append);
    tcase_add_test(tc_core, test_string_set);
    tcase_add_test(tc_core, test_string_equals);
    tcase_add_test(tc_core, test_string_clone);
    suite_add_tcase(s, tc_core);

    return s;
}
