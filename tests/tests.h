/**
 * libaul
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#ifndef __TEST_LIBAUL_H__
#define __TEST_LIBAUL_H__

#include <check.h>

typedef struct {
    int x;
    int y;
} Point;

Point*
point_new(int x, int y);

void
point_free(void *point);


Suite *
make_list_suite(void);

Suite *
make_hash_suite(void);

Suite *
make_string_suite(void);


void
make_list_tcase_insert(Suite *s);

void
make_list_tcase_remove(Suite *s);

void
make_list_tcase_iterate(Suite *s);

void
make_list_tcase_operations(Suite *s);

void
make_list_tcase_filters(Suite *s);


void
make_hash_tcase_insert(Suite *s);

#endif
