/**
 * libaul
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#include <stdlib.h>
#include <check.h>

#include "tests.h"
#include "../src/aul.h"


/**
 * Creates a new MHash
 */
START_TEST(test_hash_create)
{
    Hash *hash = hash_new();

    ck_assert_ptr_nonnull(hash);
    ck_assert_int_eq(hash_len(hash), 0);
    ck_assert_int_eq(hash_is_empty(hash), 1);

    hash_free(&hash);
    ck_assert_ptr_null(hash);
}
END_TEST

/**
 * Put some values in a hash
 */
START_TEST(test_hash_put_values)
{
    Node *node = NULL;
    Hash *hash = hash_new();

    ck_assert_ptr_nonnull(hash);
    ck_assert_int_eq(hash_len(hash), 0);

    hash_put(hash, "key1", "value 1", sizeof(char*), NULL);
    hash_put(hash, "key2", "value 2", sizeof(char*), NULL);
    hash_put(hash, "key3", "value 3", sizeof(char*), NULL);

    ck_assert_int_eq(hash_len(hash), 3);

    node = hash_get(hash, "key1");
    ck_assert_str_eq((char*) node->data, "value 1");
    node = hash_get(hash, "key2");
    ck_assert_str_eq((char*) node->data, "value 2");
    node = hash_get(hash, "key3");
    ck_assert_str_eq((char*) node->data, "value 3");

    ck_assert_ptr_null(hash_get(hash, "noop"));

    hash_free(&hash);
}
END_TEST

/**
 * Update values
 */
START_TEST(test_hash_update_values)
{
    Node *node = NULL;
    Hash *hash = hash_new();

    ck_assert_ptr_nonnull(hash);
    ck_assert_int_eq(hash_len(hash), 0);

    hash_put(hash, "key1", "value 1", sizeof(char*), NULL);
    ck_assert_int_eq(hash_len(hash), 1);

    node = hash_get(hash, "key1");
    ck_assert_str_eq((char*) node->data, "value 1");

    hash_put(hash, "key1", "value 7", sizeof(char*), NULL);
    ck_assert_int_eq(hash_len(hash), 1);

    node = hash_get(hash, "key1");
    ck_assert_str_eq((char*) node->data, "value 7");

    hash_free(&hash);
}
END_TEST

/**
 * Remove values
 */
START_TEST(test_hash_remove_values)
{
    Hash *hash = hash_new();
    int success = 0;

    ck_assert_ptr_nonnull(hash);
    ck_assert_int_eq(hash_len(hash), 0);

    hash_put(hash, "key1", "value 1", sizeof(char*), NULL);
    hash_put(hash, "key2", "value 2", sizeof(char*), NULL);
    hash_put(hash, "key3", "value 3", sizeof(char*), NULL);

    ck_assert_int_eq(hash_len(hash), 3);

    success = hash_remove(hash, "key2");

    ck_assert_int_eq(success, 1);
    ck_assert_int_eq(hash_len(hash), 2);
    ck_assert_ptr_null(hash_get(hash, "key2"));
    ck_assert_ptr_nonnull(hash_get(hash, "key3"));

    success = hash_remove(hash, "key1");

    ck_assert_int_eq(success, 1);
    ck_assert_int_eq(hash_len(hash), 1);
    ck_assert_ptr_null(hash_get(hash, "key1"));
    ck_assert_ptr_nonnull(hash_get(hash, "key3"));

    success = hash_remove(hash, "key3");

    ck_assert_int_eq(success, 1);
    ck_assert_int_eq(hash_len(hash), 0);
    ck_assert_ptr_null(hash_get(hash, "key3"));

    hash_free(&hash);
}
END_TEST

int
_test_hash_iterator(Node *node, Hash *hash, Hash *new_hash, void *user_data)
{
    char value[8];
    int *i = (int*) user_data;

    sprintf(value, "value %d", *i);

    ck_assert_ptr_nonnull(node->data);
    ck_assert_str_eq(((char*) node->data), value);

    // Check the integer is incrementing outside this function
    (*i)++;

    // Continue with the loop
    return 0;
}

/**
 * Iterate
 */
START_TEST(test_hash_iterate)
{
    Hash *hash = hash_new();
    int i = 1;

    ck_assert_ptr_nonnull(hash);
    ck_assert_int_eq(hash_len(hash), 0);

    hash_put(hash, "key1", "value 1", sizeof(char*), NULL);
    hash_put(hash, "key2", "value 2", sizeof(char*), NULL);
    hash_put(hash, "key3", "value 3", sizeof(char*), NULL);

    ck_assert_int_eq(hash_len(hash), 3);

    hash_foreach(hash, _test_hash_iterator, &i);

    // BTW, let's check that i has been incrementing
    // its value from inside the callback.
    ck_assert_int_eq(i, 4);

    hash_free(&hash);
}
END_TEST

void
_test_hash_free_point(void *data)
{
    Point *point = (Point*) data;
    ck_assert_ptr_nonnull(point);
    ck_assert_int_eq(point->x, 4);
    ck_assert_int_eq(point->y, 3);
    point_free(point);
}

int
_test_hash_point_iterator(Node *node, Hash *hash, Hash *new_hash, void *user_data)
{
    Point *point = (Point*) node->data;
    ck_assert_ptr_nonnull(point);
    ck_assert_int_eq(point->x, 4);
    ck_assert_int_eq(point->y, 3);
    return 0;
}

/**
 * Pointers and structs
 */
START_TEST(test_hash_put_structures)
{
    Hash *hash = hash_new();
    ck_assert_ptr_nonnull(hash);
    ck_assert_int_eq(hash_len(hash), 0);

    Point p1;
    p1.x = 4;
    p1.y = 3;

    Point *p2 = point_new(4, 3);

    hash_put(hash, "point1", &p1, sizeof(Point*), NULL);
    hash_put(hash, "point2", p2, sizeof(Point*), _test_hash_free_point);

    ck_assert_int_eq(hash_len(hash), 2);

    hash_foreach(hash, _test_hash_point_iterator, NULL);

    hash_free(&hash);
}
END_TEST

/**
 * Get the keys
 */
START_TEST(test_hash_get_keys)
{
    Hash *hash = hash_new();
    List *list = NULL;

    ck_assert_ptr_nonnull(hash);
    ck_assert_int_eq(hash_len(hash), 0);

    hash_put(hash, "key1", "value 1", sizeof(char*), NULL);
    hash_put(hash, "key2", "value 2", sizeof(char*), NULL);
    hash_put(hash, "key3", "value 3", sizeof(char*), NULL);

    ck_assert_int_eq(hash_len(hash), 3);

    list = hash_keys(hash);
    ck_assert_ptr_nonnull(list);
    ck_assert_int_eq(list_len(hash), 3);

    ck_assert_str_eq((char*) node_value(list_node_nth(list, 0)), "key1");
    ck_assert_str_eq((char*) node_value(list_node_nth(list, 1)), "key2");
    ck_assert_str_eq((char*) node_value(list_node_nth(list, 2)), "key3");

    hash_free(&hash);
    list_free(&list);
}
END_TEST

/**
 * Get the values
 */
START_TEST(test_hash_get_values)
{
    Hash *hash = hash_new();
    List *list = NULL;

    ck_assert_ptr_nonnull(hash);
    ck_assert_int_eq(hash_len(hash), 0);

    hash_put(hash, "key1", "value 1", sizeof(char*), NULL);
    hash_put(hash, "key2", "value 2", sizeof(char*), NULL);
    hash_put(hash, "key3", "value 3", sizeof(char*), NULL);

    ck_assert_int_eq(hash_len(hash), 3);

    list = hash_values(hash);
    ck_assert_ptr_nonnull(list);
    ck_assert_int_eq(list_len(hash), 3);

    ck_assert_str_eq((char*) node_value(list_node_nth(list, 0)), "value 1");
    ck_assert_str_eq((char*) node_value(list_node_nth(list, 1)), "value 2");
    ck_assert_str_eq((char*) node_value(list_node_nth(list, 2)), "value 3");

    hash_free(&hash);
    list_free(&list);
}
END_TEST

int
_hash_compare_func_char(void *needle, void *item)
{
    char *str1 = (char*) needle;
    char *str2 = (char*) item;

    return strcmp(str1, str2) == 0;
}

/**
 * Search for values
 */
START_TEST(test_hash_search)
{
    Hash *hash = hash_new();
    Node *node = NULL;

    ck_assert_ptr_nonnull(hash);
    ck_assert_int_eq(hash_len(hash), 0);

    hash_put(hash, "key1", "value 1", sizeof(char*), NULL);
    hash_put(hash, "key2", "value 2", sizeof(char*), NULL);
    hash_put(hash, "key3", "value 3", sizeof(char*), NULL);

    ck_assert_int_eq(hash_len(hash), 3);

    node = hash_search(hash, "value 2", _hash_compare_func_char);
    ck_assert_ptr_nonnull(node);
    ck_assert_str_eq((char*) node_value(node), "value 2");

    node = hash_search(hash, "not_found", _hash_compare_func_char);
    ck_assert_ptr_null(node);

    hash_free(&hash);
}
END_TEST

int
_test_hash_filter_iterator(Node *node, Hash *hash, Hash *new_hash, void *user_data)
{
    char *filter = (char*) user_data;
    char *value = (char*) node_value(node);

    // printf("%s =? %s -> %d\n", value, filter, strcmp(value, filter));

    return strcmp(value, filter) != 0;
}

/**
 * Filter values
 */
START_TEST(test_hash_filter)
{
    Hash *hash = hash_new();
    Hash *filtered = NULL;

    ck_assert_ptr_nonnull(hash);
    ck_assert_int_eq(hash_len(hash), 0);

    hash_put(hash, "key1", "value 1", sizeof(char*), NULL);
    hash_put(hash, "key2", "foobar", sizeof(char*), NULL);
    hash_put(hash, "key3", "value 3", sizeof(char*), NULL);
    hash_put(hash, "key4", "foobar", sizeof(char*), NULL);

    ck_assert_int_eq(hash_len(hash), 4);

    filtered = hash_filter(hash, _test_hash_filter_iterator, "foobar");
    ck_assert_ptr_nonnull(filtered);
    ck_assert_int_eq(hash_len(filtered), 2);

    ck_assert_str_eq((char*) node_value(list_node_nth(filtered, 0)), "foobar");
    ck_assert_str_eq((char*) node_value(list_node_nth(filtered, 1)), "foobar");

    // They keep the keys
    ck_assert_str_eq((char*) node_value(hash_get(filtered, "key2")), "foobar");
    ck_assert_str_eq((char*) node_value(hash_get(filtered, "key4")), "foobar");

    hash_free(&hash);
    hash_free(&filtered);
}
END_TEST

Node*
_test_hash_map(Node *node, Hash *hash, Hash *new_hash, void *user_data)
{
    int value = *(int*) user_data;
    int item = *(int*) node_value(node);
    int result = item * value;

    Node *aux = node_new(&result, sizeof(int*), NULL);
    aux->key = node->key;

    // NOTE: So the data attribtue gets cloned and we don't mess up with the variable result.
    Node *new_node = node_clone(aux);
    node_free(&aux);

    return new_node;
}

/**
 * Map nodes
 */
START_TEST(test_hash_map)
{
    Hash *hash = hash_new();
    Hash *mapped = NULL;
    int nums[] = {1, 2, 3, 4, 5, 6};
    int user_data = 2;

    ck_assert_ptr_nonnull(hash);
    ck_assert_int_eq(hash_len(hash), 0);

    hash_put(hash, "key1", &nums[0], sizeof(int*), NULL);
    hash_put(hash, "key2", &nums[1], sizeof(int*), NULL);
    hash_put(hash, "key3", &nums[2], sizeof(int*), NULL);
    hash_put(hash, "key4", &nums[3], sizeof(int*), NULL);
    hash_put(hash, "key5", &nums[4], sizeof(int*), NULL);
    hash_put(hash, "key6", &nums[5], sizeof(int*), NULL);

    ck_assert_int_eq(hash_len(hash), 6);

    mapped = hash_map(hash, _test_hash_map, &user_data);
    ck_assert_ptr_nonnull(mapped);
    ck_assert_int_eq(hash_len(mapped), 6);

    ck_assert_int_eq(*((int*) node_value(hash_get(mapped, "key1"))), 2);
    ck_assert_int_eq(*((int*) node_value(hash_get(mapped, "key2"))), 4);
    ck_assert_int_eq(*((int*) node_value(hash_get(mapped, "key3"))), 6);
    ck_assert_int_eq(*((int*) node_value(hash_get(mapped, "key4"))), 8);
    ck_assert_int_eq(*((int*) node_value(hash_get(mapped, "key5"))), 10);
    ck_assert_int_eq(*((int*) node_value(hash_get(mapped, "key6"))), 12);

    hash_free(&hash);
    hash_free(&mapped);
}
END_TEST

/**
 * Filter values
 */
START_TEST(test_hash_unique)
{
    Hash *hash = hash_new();
    Hash *unique = NULL;

    ck_assert_ptr_nonnull(hash);
    ck_assert_int_eq(hash_len(hash), 0);

    hash_put(hash, "key1", "value 1", sizeof(char*), NULL);
    hash_put(hash, "key2", "foobar", sizeof(char*), NULL);
    hash_put(hash, "key3", "value 3", sizeof(char*), NULL);
    hash_put(hash, "key4", "foobar", sizeof(char*), NULL);

    ck_assert_int_eq(hash_len(hash), 4);

    unique = hash_unique(hash, _hash_compare_func_char);
    ck_assert_ptr_nonnull(unique);
    ck_assert_int_eq(hash_len(unique), 3);

    // They keep the keys
    ck_assert_str_eq((char*) node_value(hash_get(unique, "key1")), "value 1");
    ck_assert_str_eq((char*) node_value(hash_get(unique, "key2")), "foobar");
    ck_assert_str_eq((char*) node_value(hash_get(unique, "key3")), "value 3");

    hash_free(&hash);
    hash_free(&unique);
}
END_TEST


Suite *
make_hash_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("Hashes");

    /* Core test case */
    tc_core = tcase_create("Hashes");

    tcase_add_test(tc_core, test_hash_create);
    tcase_add_test(tc_core, test_hash_put_values);
    tcase_add_test(tc_core, test_hash_update_values);
    tcase_add_test(tc_core, test_hash_remove_values);
    tcase_add_test(tc_core, test_hash_iterate);
    tcase_add_test(tc_core, test_hash_put_structures);
    tcase_add_test(tc_core, test_hash_get_keys);
    tcase_add_test(tc_core, test_hash_get_values);
    tcase_add_test(tc_core, test_hash_search);
    tcase_add_test(tc_core, test_hash_filter);
    tcase_add_test(tc_core, test_hash_map);
    tcase_add_test(tc_core, test_hash_unique);
    suite_add_tcase(s, tc_core);

    return s;
}
