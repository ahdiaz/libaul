/**
 * libaul
 *
 * @author: Antonio Hernandez <ahdiaz@gmail.com>
 */

#include <stdlib.h>
#include <check.h>

#include "../src/aul.h"


/**
 * Creates a new MNode
 */
START_TEST(test_list_remove)
{
    List *list = NULL;
    Node *node = NULL;
    int success = 0;

    list = list_new();
    ck_assert_ptr_nonnull(list);

    list_append(list, "value1", 0, NULL);
    list_append(list, "value2", 0, NULL);
    list_append(list, "value3", 0, NULL);

    ck_assert_int_eq(list_len(list), 3);

    // Let's remove the second node
    node = list_node_nth(list, 1);

    success = list_remove(node);
    ck_assert_int_eq(success, 1);
    ck_assert_ptr_nonnull(list);
    ck_assert_int_eq(list_len(list), 2);

    node = list_node_nth(list, 0);

    // This will leave us with only "value3"
    success = list_remove(node);
    ck_assert_int_eq(success, 1);
    ck_assert_int_eq(list_len(list), 1);
    ck_assert_str_eq(node_value(list_first(list)), "value3");

    node = list_node_nth(list, 0);

    success = list_remove(node);
    ck_assert_int_eq(success, 1);
    ck_assert_int_eq(list_len(list), 0);
    ck_assert_ptr_nonnull(list);

    list_free(&list);
    ck_assert_ptr_null(list);
}
END_TEST

/**
 * Creates a new MNode
 */
START_TEST(test_list_remove_nth)
{
    List *list = NULL;
    int success = 0;

    list = list_new();
    ck_assert_ptr_nonnull(list);

    list_append(list, "value1", 0, NULL);
    list_append(list, "value2", 0, NULL);
    list_append(list, "value3", 0, NULL);
    list_append(list, "value4", 0, NULL);

    ck_assert_int_eq(list_len(list), 4);

    // Out bound indexes do nothing.
    list_remove_nth(list, 77);
    ck_assert_int_eq(list_len(list), 4);
    list_remove_nth(list, -77);
    ck_assert_int_eq(list_len(list), 4);

    // Let's remove the second node.
    success = list_remove_nth(list, 1);
    ck_assert_int_eq(success, 1);
    ck_assert_int_eq(list_len(list), 3);

    // Now the first. This will leave us with value3 - value4
    success = list_remove_nth(list, 0);
    ck_assert_int_eq(success, 1);
    ck_assert_int_eq(list_len(list), 2);

    // Now the last. This will leave us with only "value3"
    success = list_remove_nth(list, 1);
    ck_assert_int_eq(success, 1);
    ck_assert_int_eq(list_len(list), 1);
    ck_assert_str_eq(list_value_nth(list, 0), "value3");

    success = list_remove_nth(list, 0);
    ck_assert_int_eq(success, 1);
    ck_assert_int_eq(list_len(list), 0);
    ck_assert_ptr_nonnull(list);

    list_free(&list);
    ck_assert_ptr_null(list);
}
END_TEST

void
make_list_tcase_remove(Suite *s)
{
    TCase *tc;

    tc = tcase_create("Remove nodes");
    tcase_add_test(tc, test_list_remove);
    tcase_add_test(tc, test_list_remove_nth);

    suite_add_tcase(s, tc);
}
