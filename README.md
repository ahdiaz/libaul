## Libaul

Libaul is an implementation of three basic structures: strings, lists and hashes. Possibly every C programmer has his own implementation of these structures, when it comes to reuse code is way to go.
I used this library extensively while programming the [RedDwarf](https://gitlab.com/ahdiaz/reddwarf) HTTP server.

Find the repository here https://gitlab.com/ahdiaz/libaul.

### Compiling and installing

The usual way. Try <em>./configure -h</em> to see available options.
In the case you want to run the tests you need to install [check](https://libcheck.github.io/check/).

    $ ./configure [--prefix=/path]
    $ make
    $ make test
    $ make install

### Your first program

It demonstrate how simple is to work with strings, note the inclusion of the header.

    #include <stdio.h>
    #include <aul.h> /* Include libaul */

    int
    main(int argc, char **argv)
    {
        String *str;

        str = str_new();
        str_set(str, "The path of the righteous man is beset");

        /* Append as many characters as indicated. */
        str_nappend(str, " on all sides by the iniquities ", 32);
        str_append(str, "of the selfish and the tyranny of evil men.");

        /* Functions to retrieve the length and value. */
        printf("(%d) - %s\n", str_len(str), str_val(str));

        /* Don't forget to free the pointer. */
        free(str);

        return 0;
    }

