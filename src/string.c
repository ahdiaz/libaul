/**
 *  libaul
 *
 *  @author: Antonio Hernandez <ahdiaz@gmail.com>
 *
 *  Copyright (C) 2017  Antonio Hernandez <ahdiaz@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
//#include <stdarg.h>
#include <string.h>
#include "aul.h"


/**
 * Initialize a new buffer
 *
 * @return String*
 */
String*
str_new()
{
    String *str = calloc(1, sizeof(String));

    if (str == NULL) {
        perror("Could not allocate memory");
        return NULL;
    }

    str->str = NULL;
    str->len = 0;
    str = str_clear(str);

    return str;
}

/**
 * Initialize a new buffer with a string
 *
 * @param char*
 * @return String*
 */
String*
str_new_set(char *str)
{
    String *sb = str_new();
    if (str != NULL) {
        sb = str_set(sb, str);
    }
    return sb;
}

/**
 * Frees a buffer
 *
 * @param String**
 */
void
str_free(String **str)
{
    if (*str != NULL) {
        free((*str)->str);
        free(*str);
        *str = NULL;
    }
}

/**
 * Returns a copy of the buffer's value
 *
 * @param String*
 * @return char*
 */
char*
str_value(String *str)
{
    if (str == NULL) {
        return NULL;
    }

    char *value = calloc(str->len + 1, sizeof(char));
    memmove(value, str->str, str->len);

    return value;
}

/**
 * Returns the buffer's size
 *
 * @param String*
 * @return size_t
 */
size_t
str_len(String *str)
{
    if (str == NULL) {
        return 0;
    }
    return str->len;
}

/**
 * Sets a buffer to an empty string
 *
 * @param String*
 * @return String*
 */
String*
str_clear(String *str)
{
    if (str == NULL) {
        return NULL;
    }

    if (str->str != NULL) {
        free(str->str);
        str->str = NULL;
    }

    str->str = calloc(1, sizeof(char));
    if (str->str == NULL) {
        perror("Could not allocate memory");
        return NULL;
    }

    str->str[0] = '\0';
    str->len = 0;

    return str;
}

/**
 * Appends the specified bytes of a string to a buffer
 *
 * @param String*
 * @param char*
 * @param size_t
 * @return String*
 */
String*
str_nappend(String *str, char *src, size_t size)
{
    if (str == NULL) {
        return NULL;
    }

    if (src == NULL) {
        return str;
    }

    size_t new_size = str->len + size;

    if (str->str == NULL) {

        str->str = calloc(new_size + 1, sizeof(char));

    } else {

        str->str = realloc(str->str, sizeof(char) * new_size + 1);
    }

    if (str->str == NULL) {
        perror("Could not allocate memory");
        return NULL;
    }

    memcpy(str->str + str->len, src, size);
    str->str[new_size] = '\0';
    str->len = new_size;

    return str;
}

/**
 * Appends a string to a buffer
 *
 * @param String*
 * @param char*
 * @return String*
 */
String*
str_append(String *str, char *src)
{
    return str_nappend(str, src, strlen(src));
}

/**
 * Sets a buffer's content to the specified number of bytes of a string
 *
 * @param String*
 * @param char*
 * @param size_t
 * @return String*
 */
String*
str_nset(String *dest, char *src, size_t size)
{
    if (dest == NULL) {
        return NULL;
    }

    if (src == NULL) {
        return dest;
    }

    if (dest->str != NULL) {
        free(dest->str);
        dest->str = NULL;
    }

    dest->str = calloc(size + 1, sizeof(char*));
    memcpy(dest->str, src, size);
    dest->len = size;

    return dest;
}

/**
 * Sets a buffer's content to a string
 *
 * @param String*
 * @param char*
 * @return String*
 */
String*
str_set(String *dest, char *src)
{
    return str_nset(dest, src, strlen(src));
}

/**
 * Clones a String buffer
 *
 * @param String*
 * @return String*
 */
String*
str_clone(String *str)
{
    if (str == NULL) {
        return NULL;
    }

    String *dest = str_new_set(str->str);

    return dest;
}

/**
 * Returns 1 if both buffers' content are equal
 *
 * @param String*
 * @param String*
 * @return int
 */
int
str_equals(String *str_a, String *str_b)
{
    if (str_a == NULL || str_b == NULL) {
        return 0;
    }

    if (strcmp(str_a->str, str_b->str) == 0) {
        return 1;
    } else {
        return 0;
    }
}

