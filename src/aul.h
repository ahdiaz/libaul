/**
 *  libaul
 *
 *  @author: Antonio Hernandez <ahdiaz@gmail.com>
 *
 *  Copyright (C) 2017  Antonio Hernandez <ahdiaz@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __LIBAUL_H__
#define __LIBAUL_H__

#include <stdio.h>
#include <stdlib.h>


/**
 * Alias for hash items keys
 */
typedef const char *hashkey;

/**
 * Function used to free list nodes' data.
 *
 * @param void *data
 *        A pointer pointing to the data contained in a node.
 */
typedef void (*FreeFunc)(void*);

/**
 * Function used to compare the data conatined in two nodes.
 *
 * @param void *data1
 *        A pointer pointing to the data contained in the first node.
 *
 * @param void *data2
 *        A pointer pointing to the data contained in the second node.
 *
 * @return int
 *        Returning 0 indicates that the comparison failed.
 */
typedef int (*CompareFunc)(void*, void*);


/**
 * string.h
 */

typedef struct _String {
    char  *str;
    size_t len;
} String;

String*
str_new();

String*
str_new_set(char *str);

void
str_free(String **str);

char*
str_value(String *str);

size_t
str_len(String *str);

String*
str_clear(String *str);

String*
str_append(String *dest, char *src);

String*
str_nappend(String *dest, char *src, size_t size);

String*
str_set(String *dest, char *src);

String*
str_nset(String *dest, char *src, size_t size);

String*
str_clone(String *str);

int
str_equals(String *str_a, String *str_b);


/**
 * node.h
 */

/**
 * The node definition
 */
typedef struct _Node {
    struct _List *list;
    struct _Node *prev;
    struct _Node *next;
    hashkey       key;
    void         *data;
    size_t        d_size;
    FreeFunc      freeFunc;
} Node;

#define node_value(node)        ((node) ? ((void*)((Node *)(node))->data) : NULL)

Node*
node_new(void *data, size_t d_size, FreeFunc freeFunc);

void
node_free(Node **node);

void*
node_data_clone(void *data, size_t size);

Node*
node_clone(Node *node);

void
node_print(Node *node);


/**
 * list.h
 */

/**
 * The list definition
 */
typedef struct _List {
    struct _Node *first;
    struct _Node *last;
    size_t        len;
    FreeFunc      freeFunc;
} List;

/**
 * Function used to iterate through the nodes of a list.
 * Additionally can be used to filter lists.
 *
 * @param Node *node
 *        The node being iterated.
 *
 * @param List *list
 *        The list the node belongs to.
 *
 * @param List *new_list
 *        The new list being formed by the caller.
 *
 * @param void *user_data
 *        A pointer pointing to arbitrary data specified by the user.
 *
 * @return int
 *        Returning 0 indicates that the loop may continue or that
 *        a filter function must include this node.
 */
typedef int (*ListIteratorFunc)(Node*, List*, List*, void*);

/**
 * Function used to map nodes of a list. The returned node
 * will be included in the new list.
 *
 * @param Node *node
 *        The node being transformed.
 *
 * @param List *list
 *        The list the node belongs to.
 *
 * @param List *new_list
 *        The new list being formed by the caller.
 *
 * @param void *user_data
 *        A pointer pointing to arbitrary data specified by the user.
 *
 * @return Node*
 *        The new created node.
 */
typedef Node* (*ListMapIteratorFunc)(Node*, List*, List*, void*);

// To be implemented
typedef int (*SortFunc)(void*, void*);


#define list_first(list)    	((list) ? ((List *)(list))->first : NULL)
#define list_prev(node)     	((node) ? ((Node *)(node))->prev : NULL)
#define list_next(node)    	    ((node) ? ((Node *)(node))->next : NULL)
#define list_last(list)         ((list) ? ((List *)(list))->last : NULL)
#define list_has_prev(node)     ((node) && ((Node *)(node))->prev ? 1 : 0)
#define list_has_next(node)     ((node) && ((Node *)(node))->next ? 1 : 0)
#define list_len(list)          ((list) ? ((List *)(list))->len : 0)
#define list_is_first(node)     ((node) && (node) == ((List *)((Node *)(node))->list)->first ? 1 : 0)
#define list_is_last(node)      ((node) && (node) == ((List *)((Node *)(node))->list)->last ? 1 : 0)
#define list_from_node(node)    ((node) ? ((Node *)(node))->list : NULL)
#define list_is_empty(list)     ((list) ? ((List *)(list))->len == 0 : 1)


List*
list_new();

List*
list_new_ff(FreeFunc freeFunc);

void
list_free(List **list);

Node*
list_append(List *list, void *data, size_t d_size, FreeFunc freeFunc);

Node*
list_append_node(List *list, Node *node);

Node*
list_prepend(List *list, void *data, size_t d_size, FreeFunc freeFunc);

Node*
list_prepend_node(List *list, Node *node);

Node*
list_insert(List *list, void *data, size_t d_size, long index, FreeFunc freeFunc);

Node*
list_insert_node(List *list, Node *node, long index);

int
list_remove(Node *node);

int
list_remove_nth(List *list, long index);

int
list_empty(List *list);

void
list_foreach(List *list, ListIteratorFunc iterator, void *user_data);

Node*
list_node_nth(List *list, long index);

void*
list_value_nth(List *list, long index);

size_t
list_index_of(List *list, void *data, size_t startindex, CompareFunc compareFunc);

Node*
list_search(List *list, void *data, size_t startindex, CompareFunc compareFunc);

List*
list_filter(List *list, ListIteratorFunc filterFunc, void *user_data);

List*
list_map(List *list, ListMapIteratorFunc mapFunc, void *user_data);

List*
list_clone(List *list);

List*
list_reverse(List *list);

List*
list_unique(List *list, CompareFunc compareFunc);

List*
list_diff(List *list1, List *list2, CompareFunc compareFunc);

List*
list_intersect(List *list1, List *list2, CompareFunc compareFunc);

List*
list_merge(List *list1, List *list2);

List*
list_sort(List *list, SortFunc iterator, void *user_data);


/**
 * hash.h
 */

/**
 * Alias for hashes
 */
typedef struct _List Hash;

/**
 * Function used to iterate through the nodes of a hash.
 * Additionally can be used to filter hashes.
 *
 * @param Node *node
 *        The node being iterated.
 *
 * @param Hash *hash
 *        The list the node belongs to.
 *
 * @param Hash *new_hash
 *        The new list being formed by the caller.
 *
 * @param void *user_data
 *        A pointer pointing to arbitrary data specified by the user.
 *
 * @return int
 *        Returning 0 indicates that the loop may continue or that
 *        a filter function must include this node.
 */
typedef int (*HashIteratorFunc)(Node*, Hash*, Hash*, void*);

/**
 * Function used to map nodes of a hash. The returned node
 * will be included in the new hash.
 *
 * @param Node *node
 *        The node being transformed.
 *
 * @param Hash *hash
 *        The list the node belongs to.
 *
 * @param Hash *new_hash
 *        The new list being formed by the caller.
 *
 * @param void *user_data
 *        A pointer pointing to arbitrary data specified by the user.
 *
 * @return Node*
 *        The new created node.
 */
typedef Node* (*HashMapIteratorFunc)(Node*, Hash*, Hash*, void*);


#define hash_first(hash)        list_first(hash)
#define hash_last(hash)         list_last(hash)
#define hash_next(node)         list_next(node)
#define hash_prev(node)         list_prev(node)
#define hash_has_prev(node)     list_has_prev(node)
#define hash_has_next(node)     list_has_next(node)
#define hash_len(hash)          list_len(hash)
#define hash_is_first(node)     list_is_first(node)
#define hash_is_last(node)      list_is_last(node)
#define hash_from_node(node)    list_from_node(node)
#define hash_is_empty(list)     list_is_empty(hash)


Hash*
hash_new_ff(FreeFunc freeFunc);

Hash*
hash_new();

Hash*
hash_clone(Hash *hash);

void
hash_free(Hash **hash);

int
hash_remove(Hash *hash, hashkey key);

int
hash_empty(Hash *hash);

Node*
hash_get(Hash *hash, hashkey key);

Node*
hash_put(Hash *hash, hashkey key, void *data, size_t d_size, FreeFunc freeFunc);

Node*
hash_put_node(Hash *hash, hashkey key, Node *node);

List*
hash_keys(Hash *hash);

List*
hash_values(Hash *hash);

void
hash_foreach(Hash *hash, HashIteratorFunc iterator, void *user_data);

Node*
hash_search(Hash *hash, void *data, CompareFunc compareFunc);

Hash*
hash_filter(Hash *hash, HashIteratorFunc filterFunc, void *user_data);

Hash*
hash_map(Hash *hash, HashMapIteratorFunc mapFunc, void *user_data);

Hash*
hash_unique(Hash *hash, CompareFunc compareFunc);

#endif /* __LIBAUL_H__ */
