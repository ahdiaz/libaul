/**
 *  libaul
 *
 *  @author: Antonio Hernandez <ahdiaz@gmail.com>
 *
 *  Copyright (C) 2017  Antonio Hernandez <ahdiaz@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "aul.h"


/**
 * Initializes a Hash structure.
 * The FreeFunc will be called by hash_free(). It will be applied
 * to all the nodes of the hash in the case the node structure has
 * none assigned.
 *
 * @param FreeFunc
 * @return Hash*
 */
Hash*
hash_new_ff(FreeFunc freeFunc)
{
    Hash *hash = list_new_ff(freeFunc);
    return hash;
}

/**
 * Creates a new Hash.
 *
 * Each of the nodes of the hash can have its own free function.
 *
 * @return Hash*
 */
Hash*
hash_new()
{
    Hash *hash = list_new();
    return hash;
}

/**
 * Clones a hash.
 *
 * @param Hash*
 * @return Hash*
 */
Hash*
hash_clone(Hash *hash)
{
    return list_clone(hash);
}

/**
 * Frees a hash.
 *
 * The hash pointer is freed in this same call.
 *
 * @param Hash**
 * @return void
 */
void
hash_free(Hash **hash)
{
    list_free(hash);
}

/**
 * Removes an element from a hash.
 *
 * This returns 1 if the node could be removed, 0 otherwise.
 *
 * @param Hash*
 * @param hashkey
 * @return int
 */
int
hash_remove(Hash *hash, hashkey key)
{
    if (!hash) {
        return 0;
    }

    Node *node = hash_get(hash, key);

    if (node) {
        list_remove(node);
    }

    return 1;
}

/**
 * Empties the complete hash.
 * Return 1 in the case of success.
 *
 * @param Hash*
 * @return int
 */
int
hash_empty(Hash *hash)
{
    return list_empty(hash);
}

/**
 * Returns the node which have the specified key.
 *
 * @param Hash
 * @param hashkey
 * @return Node*
 */
Node*
hash_get(Hash *hash, hashkey key)
{
    if (!hash) {
        return NULL;
    }

    Node *node = hash_first(hash);

    while (node) {
        if (strcmp(node->key, key) == 0) {
            break;
        }
        node = hash_next(node);
    }

    return node;
}

/**
 * Inserts or updates a node into the hash and returns it.
 *
 * @param Hash
 * @param hashkey
 * @param Node*
 * @return Node*
 */
Node*
hash_put_node(Hash *hash, hashkey key, Node *new_node)
{
    if (!hash) {
        return NULL;
    }

    Node *node = hash_get(hash, key);

    if (!node) {

        node = list_append_node(hash, new_node);

    } else {

        if (node->freeFunc) {
            node->freeFunc(node->data);
        } else if (node->list->freeFunc) {
            node->list->freeFunc(node->data);
        }

        node->data = node_data_clone(new_node->data, new_node->d_size);
        node->freeFunc = new_node->freeFunc;

        node_free(&new_node);
    }

    node->key = key;

    return node;
}

/**
 * Inserts or updates a node into the hash and returns it.
 *
 * @param Hash
 * @param hashkey
 * @param void*
 * @param size_t
 * @param FreeFunc
 * @return Node*
 */

Node*
hash_put(Hash *hash, hashkey key, void *data, size_t d_size, FreeFunc freeFunc)
{
    if (!hash) {
        return NULL;
    }

    Node *node = node_new(data, d_size, freeFunc);
    return hash_put_node(hash, key, node);
}

/**
 * Helper function that returns a new node with the key as value.
 *
 * @param Node*
 * @param Hash*
 * @param Hash*
 * @param void*
 * @return Node*
 */
Node*
_hash_get_keys_iterator_func(Node *node, Hash *hash, Hash *new_hash, void *user_data)
{
    Node *key = node_new((char*)node->key, sizeof(char*), NULL);
    return key;
}

/**
 * Returns a new list which values are the keys of the hash.
 *
 * @param Hash*
 * @return List*
 */
List*
hash_keys(Hash *hash)
{
    if (!hash) {
        return NULL;
    }

    List *keys = list_map(hash, _hash_get_keys_iterator_func, NULL);

    return keys;
}

/**
 * Helper function that returns a new node with its data as value.
 *
 * @param Node*
 * @param Hash*
 * @param Hash*
 * @param void*
 * @return Node*
 */
Node*
_hash_get_values_iterator_func(Node *node, Hash *hash, Hash *new_hash, void *user_data)
{
    Node *value = node_new(node->data, node->d_size, node->freeFunc);
    return value;
}

/**
 * Returns a new list which values are the values of the hash.
 *
 * @param Hash*
 * @return List*
 */
List*
hash_values(Hash *hash)
{
    if (!hash) {
        return NULL;
    }

    List *values = list_map(hash, _hash_get_values_iterator_func, NULL);

    return values;
}

/**
 * Iterates over a hash.
 *
 * Each of the hash items are passed to the IteratorFunc as well
 * as an optional user data.
 *
 * The IteratorFunc can return either 0, in which case the processing
 * will continue, or 1, in which case the loop is broken and the
 * processing finished.
 *
 * No matter which hash item is passed to this function, the loop
 * always begins by the first element.
 *
 * @param node
 *          A hash node
 *
 * @param iterator
 *          An instance of IteratorFunc
 *
 * @param user_data
 *          Arbitrary user data
 */
/**
 * Iterates over a hash using a callback function.
 *
 * Each of the hash items are passed to the HashIteratorFunc as well
 * as an optional user data.
 *
 * The IteratorFunc can return either 0, in which case the processing
 * will continue, or 1, in which case the loop is broken and the
 * processing finished.
 *
 * The callback receives the Node item, the hash the node belongs to,
 * another hash with will be NULL in this case and the user arbitrary data.
 *
 * @param Hash*
 * @param HashIteratorFunc
 * @param void*
 * @return void
 */
void
hash_foreach(Hash *hash, HashIteratorFunc iterator, void *user_data)
{
    list_foreach(hash, iterator, user_data);
}

/**
 * Returns the node that contains the requested data or NULL.
 *
 * @param Hash*
 * @param void*
 * @param CompareFunc
 * @return Node*
 */
Node*
hash_search(Hash *hash, void *data, CompareFunc compareFunc)
{
    return list_search(hash, data, 0, compareFunc);
}

/**
 * Returns a new hash who's elements have been filtered by the HashIteratorFunc function.
 *
 * The callback receives the Node item, the hash the node belongs to,
 * another hash which is the hash being created and returned later,
 * and the user arbitrary data.
 *
 * @param Hash*
 * @param HashIteratorFunc
 * @param void*
 * @return Hash*
 */
Hash*
hash_filter(Hash *hash, HashIteratorFunc filterFunc, void *user_data)
{
    return list_filter(hash, filterFunc, user_data);
}

/**
 * Returns a hash who's elements are the result of the processing performed
 * by the HashMapIteratorFunc function.
 *
 * The HashMapIteratorFunc is called for each of the elements of the hash and the
 * returned value will be appended to the new hash.
 *
 * The callback receives the Node item, the hash the node belongs to,
 * another hash which is the hash being created and returned later,
 * and the user arbitrary data.
 *
 * The callback must return a new node, not a modified parameter.
 *
 * @param Hash*
 * @param HashMapIteratorFunc
 * @param void*
 * @return Hash*
 */
Hash*
hash_map(Hash *hash, HashMapIteratorFunc mapFunc, void *user_data)
{
    return list_map(hash, mapFunc, user_data);
}

/**
 * Returns a copy of the hash where the duplicated elements have been removed.
 *
 * @param Hash*
 * @return Hash*
 */
Hash*
hash_unique(Hash *hash, CompareFunc compareFunc)
{
    return list_unique(hash, compareFunc);
}
