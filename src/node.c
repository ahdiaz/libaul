/**
 *  libaul
 *
 *  @author: Antonio Hernandez <ahdiaz@gmail.com>
 *
 *  Copyright (C) 2017  Antonio Hernandez <ahdiaz@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "aul.h"


/**
 * Initializes a Node structure.
 * The function FreeFunc will be called by node_free().
 *
 * @param void*
 * @param size_t
 * @param FreeFunc
 * @return Node*
 */
Node*
node_new(void *data, size_t d_size, FreeFunc freeFunc)
{
    Node *node = malloc(sizeof(Node));
    node->list = NULL;
    node->prev = NULL;
    node->next = NULL;
    node->key = NULL;
    node->data = data;
    node->d_size = d_size;
    node->freeFunc = freeFunc;
    return node;
}

/**
 * Frees a Node structure.
 *
 * @param Node**
 * @return void
 */
void
node_free(Node **node)
{
    if (!*node) {
        return;
    }

    if ((*node)->freeFunc) {

        (*node)->freeFunc((*node)->data);

    } else if ((*node)->list && (*node)->list->freeFunc) {

        (*node)->list->freeFunc((*node)->data);
    }

    (*node)->prev = NULL;
    (*node)->next = NULL;
    (*node)->key = NULL;
    (*node)->data = NULL;
    (*node)->list = NULL;
    (*node)->freeFunc = NULL;

    free(*node);
    *node = NULL;
}

/**
 * Returns a copy of a node data.
 * It is needed to free its memory later on.
 *
 * @param void*
 * @param d_size
 * @return void*
 */
void*
node_data_clone(void *data, size_t size)
{
    void *clone = NULL;
    clone = malloc(size + 1);
    memset(clone, '\0', size + 1);
    memcpy(clone, data, size);
    return clone;
}

/**
 * Returns a copy of a node
 *
 * @param Node*
 * @return Node*
 */
Node*
node_clone(Node *node)
{
    void *data = node_data_clone(node->data, node->d_size);
    Node *clone = node_new(data, node->d_size, node->freeFunc);
    clone->key = node->key;

    return clone;
}

/**
 * Just for debugging
 *
 * @param Node*
 * @return void*
 */
void
node_print(Node *node)
{
    printf(
        "key = %s\n&data = %p\nsize = %ld\n",
        node->key,
        node->data,
        node->d_size
    );
}
