/**
 *  libaul
 *
 *  @author: Antonio Hernandez <ahdiaz@gmail.com>
 *
 *  Copyright (C) 2017  Antonio Hernandez <ahdiaz@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "aul.h"


/**
 * Initializes a List structure.
 * The FreeFunc will be called by list_free(). It will be applied
 * to all the nodes of the list in the case the node structure has
 * none assigned.
 *
 * @param FreeFunc
 * @return List*
 */
List*
_list_new(FreeFunc freeFunc)
{
    List *list = malloc(sizeof(List));
    list->first = NULL;
    list->last = NULL;
    list->len = 0;
    list->freeFunc = freeFunc;
    return list;
}

/**
 * Frees a List structure.
 *
 * Call this function once all the list items have been
 * freed.
 *
 * @param List**
 * @return void
 */
void
_list_free(List **list)
{
    if (!*list) {
        return;
    }

    while ((*list)->first) {
        list_remove((*list)->first);
    }

    free(*list);
    *list = NULL;
}

/**
 * Creates a new Node passing a FreeFunc function.
 *
 * This is typically the case when all the items
 * are of the same data type and it's not needed
 * to always pass the same FreeFunc to each node.
 *
 * @param FreeFunc
 * @return List*
 */
List*
list_new_ff(FreeFunc freeFunc)
{
    return _list_new(freeFunc);
}

/**
 * Creates a new List.
 *
 * Each of the nodes of the list can have its own free function.
 *
 * @return List*
 */
List*
list_new()
{
    return _list_new(NULL);
}

/**
 * Frees a list.
 *
 * The list pointer is freed in this same call.
 *
 * @param List**
 * @return void
 */
void
list_free(List **list)
{
    _list_free(list);
}

/**
 * Removes an element from a list.
 *
 * This returns 1 if the node could be removed, 0 otherwise.
 *
 * @param Node*
 * @return int
 */
int
list_remove(Node *node)
{
    if (!node) {
        return 0;
    }

    List *list = node->list;

    if (node->prev) {
        node->prev->next = node->next;
    }

    if (node->next) {
        node->next->prev = node->prev;
    }

    if (list->first == node) {
        list->first = node->next;
    }

    if (list->last == node) {
        list->last = node->prev;
    }

    list->len--;

    node_free(&node);

    return 1;
}

/**
 * Removes the nth element from a list.
 *
 * This returns 1 if the node could be removed, 0 otherwise.
 *
 * @param Node*
 * @param long
 * @return int
 */
int
list_remove_nth(List *list, long index)
{
    Node *nth_node = list_node_nth(list, index);
    return list_remove(nth_node);
}

/**
 * Empties the complete list.
 * Return 1 in the case of success.
 *
 * @param List*
 * @return int
 */
int
list_empty(List *list)
{
    if (!list) {
        return 0;
    }

    Node *node = list_first(list);

    while (node) {
        list_remove(node);
        node = list_first(list);
    }

    return 1;
}

/**
 * Returns the nth element from a list.
 *
 * @param List*
 * @param long
 * @return Node*
 */
Node*
list_node_nth(List *list, long index)
{
    if (!list) {
        return NULL;
    }

    long last_index = ((long)list_len(list)) - 1;

    if (index < 0 || index > last_index) {
        return NULL;
    }

    long i = 0;
    Node *nth_node = list_first(list);

    while (i < index) {
        nth_node = list_next(nth_node);
        i++;
    }

    return nth_node;
}

/**
 * Returns the nth element's value from a list.
 * The caller is responsible of casting the returned pointer.
 *
 * @param List*
 * @param long
 * @return void*
 */
void*
list_value_nth(List *list, long index)
{
    return node_value(list_node_nth(list, index));
}

/**
 * Appends a node to the end of a list and returns the same node.
 *
 * @param List*
 * @param Node*
 * @return Node*
 */
Node*
list_append_node(List *list, Node *node)
{
    if (!list) {
        return NULL;
    }

    Node *last = list_last(list);

    if (last) {

        last->next = node;
        node->prev = last;

    } else {

        list->first = node;
    }

    node->list = list;
    list->last = node;
    list->len++;

    return node;
}

/**
 * Appends a node to the end of a list and returns the same node.
 * The FreeFunc will be called by node_free().
 *
 * @param List*
 * @param void*
 * @param d_size
 * @param FreeFunc
 * @return Node*
 */
Node*
list_append(List *list, void *data, size_t d_size, FreeFunc freeFunc)
{
    if (!list) {
        return NULL;
    }

    Node *node = node_new(data, d_size, freeFunc);
    return list_append_node(list, node);
}

/**
 * Prepends a node to the begining of a list and returns the same node.
 *
 * @param List*
 * @param Node*
 * @return Node*
 */
Node*
list_prepend_node(List *list, Node *node)
{
    if (!list) {
        return NULL;
    }

    Node *first = list_first(list);

    if (first) {

        first->prev = node;
        node->next = first;

    } else {

        list->last = node;
    }

    node->list = list;
    list->first = node;
    list->len++;

    return node;
}

/**
 * Prepends a node to the begining of a list and returns the same node.
 * The FreeFunc will be called by node_free().
 *
 * @param List*
 * @param void*
 * @param d_size
 * @param FreeFunc
 * @return Node*
 */
Node*
list_prepend(List *list, void *data, size_t d_size, FreeFunc freeFunc)
{
    if (!list) {
        return NULL;
    }

    Node *node = node_new(data, d_size, freeFunc);
    return list_prepend_node(list, node);
}

/**
 * Inserts a node in the specified position of a list and returns the same node.
 *
 * @param List*
 * @param Node*
 * @param long
 * @return Node*
 */
Node*
list_insert_node(List *list, Node *node, long index)
{
    if (!list) {
        return NULL;
    }

    long last_index = ((long)list_len(list)) - 1;

    if (index < 0) {
        return list_prepend_node(list, node);
    } else if (index >= last_index) {
        return list_append_node(list, node);
    }

    Node *nth_node = list_node_nth(list, index);

    if (nth_node == NULL) {
        // Bad index?
        node_free(&node);
        return NULL;
    }

    node->prev = nth_node;
    node->next = list_next(nth_node);

    if (list_has_next(nth_node)) {
        nth_node->next->prev = node;
    }

    nth_node->next = node;

    node->list = list;
    list->len++;

    return node;
}

/**
 * Inserts a node in the specified position of a list and returns the same node.
 * The FreeFunc will be called by node_free().
 *
 * @param List*
 * @param void*
 * @param d_size
 * @param long
 * @param FreeFunc
 * @return Node*
 */
Node*
list_insert(List *list, void *data, size_t d_size, long index, FreeFunc freeFunc)
{
    if (!list) {
        return NULL;
    }

    Node *node = node_new(data, d_size, freeFunc);
    return list_insert_node(list, node, index);
}

/**
 * Iterates over a list using a callback function.
 *
 * Each of the list items are passed to the ListIteratorFunc as well
 * as an optional user data.
 *
 * The IteratorFunc can return either 0, in which case the processing
 * will continue, or 1, in which case the loop is broken and the
 * processing finished.
 *
 * The callback receives the Node item, the list the node belongs to,
 * another list with will be NULL in this case and the user arbitrary data.
 *
 * @param List*
 * @param ListIteratorFunc
 * @param void*
 * @return void
 */
void
list_foreach(List *list, ListIteratorFunc iterator, void *user_data)
{
    if (!list) {
        return;
    }

    int cont = 1;
    Node *node = list_first(list);

    while (node && cont == 1) {
        cont = !iterator(node, list, NULL, user_data);
        node = list_next(node);
    }
}

/**
 * Helper function for basic comparison.
 * Returns 0 if the comparison fails or any other value when success.
 *
 * @param void*
 * @param void*
 * @return int
 */
int
_compare_func(void *needle, void *item)
{
    return needle == item;
}

/**
 * Finds a node which contains the data specified in the parameters.
 * It uses the default comparison function if the user didn't provide one,
 * although is likely the user will want to privide its own comparison function.
 * The search will begin at the index specified in the parameters.
 *
 * The results, node and its index, are assigned to the out parameters
 * found_node and found_index.
 *
 * @param List*
 * @param void*
 * @param size_t
 * @param CompareFunc
 * @param Node**
 * @param size_t*
 */
void
_list_find_node_and_index(List *list, void *data, size_t startindex, CompareFunc compareFunc, Node **found_node, size_t *found_index)
{
    size_t index = list_len(list) - 1;

    if (!list || startindex > index) {
        return;
    }

    int found = 0;
    Node *node = list_first(list);
    index = 0;
    CompareFunc cfx = compareFunc == NULL ? _compare_func : compareFunc;

    while (node) {

        if (index >= startindex) {

            found = cfx(data, node->data);

            if (found != 0) {

                *found_node = node;
                *found_index = index;

                return;
            }
        }

        node = list_next(node);
        index++;
    }

    return;
}

/**
 * Returns the index that contains the requested data or -1.
 *
 * @param List*
 * @param void*
 * @param size_t
 * @param CompareFunc
 * @return size_t
 */
size_t
list_index_of(List *list, void *data, size_t startindex, CompareFunc compareFunc)
{
    Node *found_node = NULL;
    size_t found_index = -1;

    _list_find_node_and_index(list, data, startindex, compareFunc, &found_node, &found_index);

    return found_index;
}

/**
 * Returns the node that contains the requested data or NULL.
 *
 * @param List*
 * @param void*
 * @param size_t
 * @param CompareFunc
 * @return Node*
 */
Node*
list_search(List *list, void *data, size_t startindex, CompareFunc compareFunc)
{
    Node *found_node = NULL;
    size_t found_index = -1;

    _list_find_node_and_index(list, data, startindex, compareFunc, &found_node, &found_index);

    return found_node;
}

/**
 * Returns a new list who's elements have been filtered by the ListIteratorFunc function.
 *
 * The callback receives the Node item, the list the node belongs to,
 * another list which is the list being created and returned later,
 * and the user arbitrary data.
 *
 * @param List*
 * @param ListIteratorFunc
 * @param void*
 * @return List*
 */
List*
list_filter(List *list, ListIteratorFunc filterFunc, void *user_data)
{
    if (!list) {
        return NULL;
    }

    List *new_list = NULL;
    Node *node = NULL, *cloned = NULL;
    int exclude = 0;

    new_list = _list_new(list->freeFunc);
    node = list_first(list);

    while (node) {

        exclude = filterFunc(node, list, new_list, user_data);

        if (exclude == 0) {
            cloned = node_clone(node);
            list_append_node(new_list, cloned);
        }

        node = list_next(node);
    }

    return new_list;
}

/**
 * Returns a list who's elements are the result of the processing performed
 * by the MapIteratorFunc function.
 *
 * The MapIteratorFunc is called for each of the elements of the list and the
 * returned value will be appended to the new list.
 *
 * The callback receives the Node item, the list the node belongs to,
 * another list which is the list being created and returned later,
 * and the user arbitrary data.
 *
 * The callback must return a new node, not a modified parameter.
 *
 * @param List*
 * @param ListMapIteratorFunc
 * @param void*
 * @return List*
 */
List*
list_map(List *list, ListMapIteratorFunc mapFunc, void *user_data)
{
    if (!list) {
        return NULL;
    }

    List *new_list = NULL;
    Node *node = NULL, *mapped_node = NULL;

    new_list = _list_new(list->freeFunc);
    node = list_first(list);

    while (node) {

        mapped_node = (Node*) mapFunc(node, list, new_list, user_data);
        list_append_node(new_list, mapped_node);

        node = list_next(node);
    }

    return new_list;
}

/**
 * Helper function that clones a node.
 *
 * @param Node*
 * @param List*
 * @param List*
 * @param void*
 * @return Node*
 */
Node*
_list_clone_func(Node *node, List *list, List *new_list, void *user_data)
{
    Node *cloned = node_clone(node);

    return cloned;
}

/**
 * Clones a list.
 *
 * @param List*
 * @return List*
 */
List*
list_clone(List *list)
{
    return list_map(list, _list_clone_func, NULL);
}

/**
 * Returns a new list with its items reversed.
 *
 * @param List*
 * @return List*
 */
List*
list_reverse(List *list)
{
    if (!list) {
        return NULL;
    }

    List *new_list = NULL;
    Node *node = NULL, *cloned = NULL;

    new_list = _list_new(list->freeFunc);
    node = list_last(list);

    while (node) {

        cloned = node_clone(node);
        list_append_node(new_list, cloned);

        node = list_prev(node);
    }

    return new_list;
}

/**
 * Returns a copy of the list where the duplicated elements have been removed.
 *
 * @param List*
 * @return List*
 */
List*
list_unique(List *list, CompareFunc compareFunc)
{
    if (!list) {
        return NULL;
    }

    int i = 0, len = 0;
    List *cloned = list_clone(list);
    Node *node = NULL, *found = NULL;
    CompareFunc func = compareFunc != NULL ? compareFunc : _compare_func;

    len = list_len(cloned);

    while (i < len) {

        node = list_node_nth(cloned, i);
        found = list_search(cloned, node->data, i + 1, func);

        if (found != NULL) {
            list_remove(found);
            len = list_len(cloned);
        }

        i++;
    }

    return cloned;
}

/**
 * Returns a new list which contains the elements that doesn't appears
 * in the second list.
 *
 * @param List*
 * @param List*
 * @param CompareFunc*
 * @return List*
 */
List*
list_diff(List *list1, List *list2, CompareFunc compareFunc)
{
    if (!list1) {
        return NULL;
    }

    if (!list2) {
        return list_clone(list1);
    }

    List *new_list = NULL;
    Node *node = NULL, *found = NULL;
    CompareFunc func = compareFunc != NULL ? compareFunc : _compare_func;

    new_list = _list_new(list1->freeFunc);
    node = list_first(list1);

    while (node) {

        found = list_search(list2, node->data, 0, func);

        if (found == NULL) {
            found = node_clone(node);
            list_append_node(new_list, found);
        }

        node = list_next(node);
    }

    return new_list;
}

/**
 * Returns a new list which only contains the elements that appears
 * in the second list.
 *
 * @param List*
 * @param List*
 * @param CompareFunc*
 * @return List*
 */
List*
list_intersect(List *list1, List *list2, CompareFunc compareFunc)
{
    if (!list1) {
        return NULL;
    }

    if (!list2) {
        return list_clone(list1);
    }

    List *new_list = NULL;
    Node *node = NULL, *found = NULL;
    CompareFunc func = compareFunc != NULL ? compareFunc : _compare_func;

    new_list = _list_new(list1->freeFunc);
    node = list_first(list1);

    while (node) {

        found = list_search(list2, node->data, 0, func);

        if (found != NULL) {
            found = node_clone(found);
            list_append_node(new_list, found);
        }

        node = list_next(node);
    }

    return new_list;
}

/**
 * Returns a new list which is the merge of the two passed as parameters.
 *
 * @param List*
 * @param List*
 * @return List*
 */
List*
list_merge(List *list1, List *list2)
{
    if (!list1 || !list2) {
        return NULL;
    }

    List *merged = list_clone(list1);

    Node *node = list_first(list2);
    Node *cloned = NULL;

    while (node) {

        cloned = node_clone(node);
        list_append_node(merged, cloned);

        node = list_next(node);
    }

    return merged;
}

/**
 * To be implemented
 */
List*
list_sort(List *list, SortFunc iterator, void *user_data)
{
    // To be implemented
    return NULL;
}
